import 'package:assessment1/editprofile.dart';
import 'package:assessment1/login.dart';
import 'package:assessment1/signup.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        centerTitle: true,
      ),

      body: Center(
        child: Column(
          children: [
            ElevatedButton(
                  onPressed: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const LogIn()))
                  },
                  child: const Text('Log In Page'),
                ),

            ElevatedButton(
                  onPressed: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const SignUp()))
                  },
                  child: const Text('Register'),
                ),

                ElevatedButton(
                  onPressed: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()))
                  },
                  child: const Text('Dashboard Page'),
                ),

                ElevatedButton(
                  onPressed: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const EditProfile()))
                  },
                  child: const Text('Edit Profile Page'),
                ),
          ],
        ), 
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          // Add your press here
          Navigator.push(context, MaterialPageRoute(builder: (context) => const EditProfile()))
        },
        child: const Icon(Icons.edit),
      ),
    );
  }
}